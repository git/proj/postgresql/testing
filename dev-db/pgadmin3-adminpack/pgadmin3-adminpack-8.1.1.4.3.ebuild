# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit multilib toolchain-funcs versionator

KEYWORDS="~x86"

PG_PV="$(get_version_component_range 1-2)"
MY_PV="$(get_version_component_range 3-)"

DESCRIPTION="PostgreSQL contrib module which implements a number of support functions which pgAdmin will use to provide additional functionality."
HOMEPAGE="http://www.pgadmin.org/"
SRC_URI="mirror://postgresql/pgadmin3/release/v${MY_PV}/adminpacks/admin${PG_PV/.}-${MY_PV}.tar.gz"
LICENSE="POSTGRESQL"
SLOT="0"
IUSE=""

DEPEND="=dev-db/postgresql-${PG_PV}*"
RDEPEND="${DEPEND}"

S=${WORKDIR}/admin${PG_PV/.}

src_compile() {
	$(tc-getCC) \
		-shared -Wl,-soname=admin${PG_PV/.}-0.0 \
		-o admin${PG_PV/.}.so \
		-fPIC \
		-I${ROOT}/usr/include/postgresql/server/ \
		admin${PG_PV/.}.c || die "compilation failed"
	
}

src_install() {
	insinto /usr/$(get_libdir)/postgresql
	doins admin${PG_PV/.}.so

	insinto /usr/share/postgresql/contrib
	sed -i \
		-e "s#MODULE_PATHNAME#/usr/$(get_libdir)/postgresql/admin${PG_PV/.}.so#" \
		admin81.sql.in || die "sed failed"
	newins admin81.sql.in admin81.sql
}

pkg_postinst() {
	elog "You have to 'activate' the module by executing some SQL statements."
	elog "This can be done with the following command:"
	elog "psql -U postgres some_database -f ${ROOT}/usr/share/postgresql/contrib/admin${PG_PV/.}.sql"
}
