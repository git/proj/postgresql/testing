# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils einput versionator

IUSE="perl doc"

MY_PV=$(replace_version_separator 3 '-')
DESCRIPTION="A replication system for the PostgreSQL Database Management System"
HOMEPAGE="http://slony.info/"
SRC_URI="http://pgfoundry.org/frs/download.php/1015/${P}.tar.bz2"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"

DEPEND="dev-db/postgresql
	perl? ( dev-perl/DBD-Pg )"

RDEPEND="dev-db/postgresql"

src_unpack() {
	unpack ${A}
}

src_compile() {
	cd ${WORKDIR}/${PN}-${MY_PV}
	local myconf=""

	myconf="${myconf} --with-pgincludedir=/usr/include/postgresql/pgsql"
	myconf="${myconf} --with-pgincludeserverdir=/usr/include/postgresql/server"
	myconf="${myconf} $(use_with perl perltools)"
	myconf="${myconf} $(use_with doc docs)"

	econf ${myconf} || die
	emake || die

	if use perl ; then
		cd ${S}/tools
		emake || die
	fi
	emake || die
	emake DESTDIR=${D} install || die
}

src_install() {
	if useq doc; then
		dodoc HISTORY-1.1 INSTALL README SAMPLE TODO UPGRADING doc/howto/*.txt
		dohtml doc/howto/*.html
	fi

	newinitd ${FILESDIR}/slony1.init slony1 || die
	newconfd ${FILESDIR}/slony1.conf slony1 || die
}

pkg_postinst() {
	einfo
	einfo "For important information regarding"
	einfo "upgrading slony1, please config this package:"
	einfo "# emerge --config 'dev-db/slony1'"
	einfo
}

pkg_config() {
	einput_add_init ${PN} "default"
	einfo
	einfo "In order to upgrade, you need to first stop"
	einfo "slon on this node"
	einfo
	if einput_confirm "Do you want to stop slony1 on this node?" "0"; then
		${ROOT}/etc/init.d/slony1 stop
	fi
	einfo
	einfo "Run the following for all nodes from a slonik script:"
	einfo "update functions (id = [whatever]);"
	einfo
	if einput_confirm "Do you want to restart slony1 on this node?" "0"; then
		${ROOT}/etc/init.d/slony1 restart
	fi
}
