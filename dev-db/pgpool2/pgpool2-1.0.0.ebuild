# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit einput

KEYWORDS="~amd64 ~ppc ~sparc ~x86"

DESCRIPTION="Connection pool for PostgreSQL that supports parallel querying."
HOMEPAGE="http://pgpool.projects.postgresql.org/"
SRC_URI="http://jawed.name/dev/gentoo/${P}.tar.bz2"
LICENSE="BSD"
SLOT="0"
IUSE=""

DEPEND="dev-db/libpq"
RDEPEND="${DEPEND}
!>=dev-db/pgpool-3"

src_unpack() {
	unpack ${A}
	cd "${S}"
	sed -i -e "/^logdir/s:/tmp:/var/run/${PN}:g" pgpool.conf.sample || die "sed failed"
	sed -i -e "/^socket_dir/s:/tmp:/var/run/${PN}:g" pgpool.conf.sample || die "sed failed"
	sed -i -e "/^pcp_socket_dir/s:/tmp:/var/run/${PN}:g" pgpool.conf.sample || die "sed failed"
	sed -i -e "/^backend_socket_dir/s:/tmp:/var/run/${PN}:g" pgpool.conf.sample || die "sed failed"
}

src_compile() {
	cd "${S}"
	econf --with-pgsql=/usr/include/postgresql || die "econf failed"
	emake || die "emake failed"
}

src_install () {
	cd "${S}"
	diropts "-m0755"
	dodir "/var/run/${PN}"
	keepdir "/var/run/${PN}"
	chown -R postgres:postgres "/var/run/${PN}"
	einstall || die "einstall failed"
	mv -f "${D}/etc/pgpool.conf.sample" "${D}/etc/pgpool.conf"
	mv -f "${D}/etc/pcp.conf.sample" "${D}/etc/pcp.conf"
	dodoc AUTHORS COPYING ChangeLog INSTALL NEWS README* TODO doc/*
	docinto sql
	dodoc sql/system-db.sql
	exeinto /etc/init.d
	newexe "${FILESDIR}/${PN}.init" ${PN}
}

pkg_config() {
	while [ 1 ];do
		einput_list "1" "Connection pooling mode" "2" "Replication mode" "3" \
		"Master/slave mode" "4" "Parallel Query Execution mode" "5" \
		"Generate md5 for pcp.conf" "6" "Add pgpool2 to default runlevel" \
		"7" "Exit" "Choice"
		
		case "${EINPUT_ANSWER}" in
			1) pgpool2_connection_pool;;
			2) pgpool2_replication_mode;;
			3) pgpool2_masterslave_mode;;
			4) pgpool2_parallel_query_mode;;
			5) pgpool2_generate_md5;;
			6) einput_add_init "pgpool2" "default";;
			7) einfo "Bye"
			exit;;
		esac
	done
}

pgpool2_generate_md5() {
	einput_prompt_secret "Please enter a password"
	checksum=`/usr/bin/pg_md5 ${EINPUT_ANSWER}`
	einfo "${checksum}"
}

pgpool2_connection_pool() {
	einfo "Connection pool mode"
	einfo "- raw mode (all clients connect to PostgreSQL through pgpool-II)"
	einfo "pgpool-II operates in connection pooling mode by default."
}

pgpool2_replication_mode() {
	einfo "Replication mode"
	einfo "- raw mode (all clients connect to PostgreSQL through pgpool-II)"
	einfo "pgpool-II operates in replication mode when the following variable"
	einfo "in /etc/pgpool.conf are set:"
	einfo "replication_mode = true"
}

pgpool2_masterslave_mode() {
	einfo "Master/slave mode"
	einfo "Use pgpool-II with another master/slave replication software (like
	Slony-I) making it do the actual data replication."
	einfo "pgpool-II operates in master/slave mode when the following variable"
	einfo "in /etc/pgpool.conf are set:"
	einfo "replication_mode = false"
	einfo "master_slave_mode = true"
}

pgpool2_parallel_query_mode() {
	einfo "Parallel Query Execution mode"
	einfo "This mode enables parallel execution of queries."
	einfo "pgpool-II operates in PQE mode when the following is executed:"
	einfo "$ psql -f /usr/share/pgpool2/sql/system_db.sql pgpool"
	einfo "Please modify this file to accordance with your DB"
}
