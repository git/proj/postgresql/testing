# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

KEYWORDS="~amd64 ~ppc ~sparc ~x86"

DESCRIPTION="Connection pool server for PostgreSQL."
HOMEPAGE="http://pgpool.projects.postgresql.org/"
SRC_URI="http://pgfoundry.org/frs/download.php/899/${P}.tar.gz"
LICENSE="BSD"
SLOT="0"
IUSE=""

DEPEND="dev-db/libpq"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	sed -i -e "/^logdir/s:/tmp:/var/run:g" pgpool.conf.sample || die "sed failed"
}

src_compile() {
	cd "${S}"
	econf --with-pgsql=/usr/include/postgresql || die "econf failed"
	emake || die "emake failed"
}

src_install () {
	cd "${S}"
	einstall || die "einstall failed"
	mv -f "${D}/etc/pgpool.conf.sample" "${D}/etc/pgpool.conf"
	dodoc AUTHORS COPYING ChangeLog INSTALL NEWS README* TODO
	exeinto /etc/init.d
	newexe "${FILESDIR}/${PN}.init" ${PN}
}
